package com.lx.entity;

import com.lx.util.LX;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 游林夕 on 2019/8/23.
 */
public class Var extends HashMap {
    public Var(){}
    public Var(String json){this.putAll(LX.toMap(json));}
    public Var(Map map){this.putAll(map);}
    public Var(Object k,Object v){this.put(k,v); }
    /** 获取字符串类型*/
    public String getStr(Object key){return LX.isEmpty(get(key))?"":get(key).toString();}
    public List<Var> getList(Object key){return LX.toList(get(key));}
    public <T>List<T> getList(Class<T> t,Object key){
        return LX.toList(t, get(key));
    }
    public Var getVar(Object key){return LX.toMap(get(key));}
    public int getInt(Object key){return (int)getDouble(key);}
    public double getDouble(Object key){return Double.parseDouble(getStr(key));}
    public BigDecimal getBigDecimal(Object key){return LX.getBigDecimal(get(key));}

    public boolean eq1(Object key){
        return "1".equals(getStr(key));
    }
    public boolean eq0(Object key){
        return "0".equals(getStr(key));
    }
    /** 获取值(可能强转不了)*/
    public <T> T getObj(Object key){return (T) get(key);}
    /** 获取指定类型值*/
    public <T> T getObj(Class<T> t,Object key){return LX.toObj(t,get(key));}

    @Override
    public Var put(Object key, Object value) {
        super.put(key, value);
        return this;
    }

    public String toJsonString() {
        return LX.toJSONString(this);
    }
}
