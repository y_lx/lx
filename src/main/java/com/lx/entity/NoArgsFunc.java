package com.lx.entity;

import com.lx.annotation.Note;

@Note("无参函数")
@FunctionalInterface
public interface NoArgsFunc {
    void exec();
}