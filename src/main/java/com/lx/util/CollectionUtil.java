package com.lx.util;//说明:


import com.lx.entity.Var;
import com.lx.gson.Gson;
import com.lx.gson.GsonBuilder;
import com.lx.gson.JsonElement;
import com.lx.gson.JsonParser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建人:游林夕/2019/3/27 15 01
 */
class CollectionUtil {
    /**将对象转为对象*/
    static <T> T toObj(Class<T> t,Object obj) {
        if (LX.isEmpty(obj)){
            return null;
        }
        if (t.isAssignableFrom(obj.getClass())){
            return (T) obj;
        }
        if (Map.class.isAssignableFrom(t)){
            return (T)toMap((Class<Map>)t, obj);
        }
        return g.fromJson(obj instanceof String?(String)obj:g.toJson(obj),t);
    }
    /**
     * 将对象转为Map
     *  @author
     *  创建时间：2018年4月26日 下午5:35:55
     */
    static <T extends Map>T toMap(Class<T> t,Object json) {
        if (json == null){
            return null;
        }
        if (t.isAssignableFrom(json.getClass())){
            return (T) json;
        }
        Map pd = g.fromJson(json instanceof String?(String)json:g.toJson(json),Map.class);
        T pp = null;
        try {
            pp = t.newInstance();
            for (Object key : pd.keySet()) {
                if(LX.isEmpty(key)){
                    continue;
                }
                Object obj = pd.get(key);
                if(LX.isNotEmpty(obj)){
                    if(obj instanceof Map){
                        pp.put(key, toMap(t,obj));
                    }else if(obj instanceof List){
                        pp.put(key, toList(obj));
                    }else {
                        pp.put(key,obj);
                    }
                }else{
                    pp.put(key, obj);
                }
            }
        } catch (Exception e) {
            LX.exMsg(e);
        }
        return pp;
    }
    public static <T>List<T> toList(Class<T> t,Object obj){
        List list = CollectionUtil.toList(obj);
        List<T> ls = new ArrayList<>();
        if (LX.isNotEmpty(list)){
            list.forEach(v->{
                if (t.isAssignableFrom(v.getClass())){
                    ls.add((T)v);
                }else{
                    ls.add(toObj(t,v));
                }
            });
        }
        return ls;
    }
    /**将对象转为List*/
    static  List toList(Object obj){
        if (obj == null){
            return null;
        }
        List list = null;
        if (List.class.isAssignableFrom(obj.getClass())){
            list = (List) obj;
        }else{
            list =  g.fromJson(obj instanceof String?(String)obj:g.toJson(obj),List.class);
        }
        List ls = new ArrayList();
        for (Object o : list){
            if(o!=null){
                if(o instanceof Map){
                    ls.add(toMap(Var.class,o));
                }else if(o instanceof List){
                    ls.add(toList(o));
                }else {
                    ls.add(o);
                }
            }else{
                ls.add(o);
            }
        }
        return ls;
    }

    private static Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().create();

    static String toJSONString(Object obj) {
        if (obj == null){
            return null;
        }
        return obj instanceof String?(String)obj:g.toJson(obj);
    }
    //说明:格式化输出json字符串
    /**{ ylx } 2019/8/12 10:04 */
    static String toFormatJson(Object json) {
        JsonElement jsonElement = null;
        if (json instanceof List || json.toString().trim().startsWith("[")){
            jsonElement = new JsonParser().parse(toJSONString(json)).getAsJsonArray();
        }else{
            jsonElement = new JsonParser().parse(toJSONString(json)).getAsJsonObject();
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
        return gson.toJson(jsonElement);
    }
}


