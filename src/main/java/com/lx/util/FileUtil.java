package com.lx.util;

import com.lx.annotation.Note;

import java.io.*;
import java.util.LinkedList;

class LXFileUtil {

    @Note("获取类路径下的文件路径")
    public static String getClassPath(Class<?> cls, String path){
        return cls.getResource("").getPath()+getPath(path);
    }

    @Note("获取resource路径下的文件路径")
    public static String getResourcePath(String path){
        return ClassLoader.getSystemResource("").getPath()+getPath(path);
    }
    @Note("获取文件输入流")
    public static InputStream getFileInputstream(String path){
        return ClassLoader.getSystemResourceAsStream(getPath(path));
    }
    //说明: 获取文件路径
    /** @author ylx 2022/12/23 9:50 */
    public static String getPath(String path){
        if (LX.isEmpty(path)) {
            return "";
        }
        return path.replaceAll("\\+", File.separator).replaceAll("/+", File.separator);
    }
}
