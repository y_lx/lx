/**
 * This package provides annotations that can be used with {@link com.lx.gson.Gson}.
 * 
 * @author Inderjeet Singh, Joel Leitch
 */
