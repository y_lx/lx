package com.lx.annotation;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * 打包成jar后看不见注释
 *
 * @author ylx
 * @date 2020-06-10
 */
@Target({TYPE,FIELD,METHOD,PARAMETER,CONSTRUCTOR,LOCAL_VARIABLE,ANNOTATION_TYPE,PACKAGE,TYPE_PARAMETER,TYPE_USE})
@Retention(RetentionPolicy.CLASS)
@Inherited
@Documented
public @interface Note {
    @Note("注释信息, 方便打包成jar后查看注释")
    String value();
}
