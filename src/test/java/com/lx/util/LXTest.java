//package com.lx.util;
//
//import com.lx.entity.ExpireCache;
//import com.lx.entity.Var;
//import com.lx.util.exception.ResultServiceException;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//import java.io.ByteArrayInputStream;
//import java.nio.charset.Charset;
//import java.nio.charset.StandardCharsets;
//import java.util.*;
//
//import static com.lx.util.SignUtil.CIPHER_ALGORITHM;
//
//class LXTest {
//
//    @org.junit.jupiter.api.Test
//    void isEmpty() {
//        Assertions.assertTrue(LX.isEmpty(null));
//        Assertions.assertTrue(LX.isEmpty(new ArrayList<>()));
//        Assertions.assertTrue(LX.isEmpty(new String[]{}));
//        Assertions.assertTrue(LX.isEmpty(new String[]{}));
//        Assertions.assertTrue(LX.isEmpty(new String[]{}));
//        Assertions.assertTrue(LX.isEmpty(new String[]{}));
//        Assertions.assertTrue(LX.isEmpty(new HashMap<>()));
//        Assertions.assertTrue(LX.isEmpty(new HashSet<>()));
//        Assertions.assertTrue(LX.isEmpty(""));
//    }
//
//    @org.junit.jupiter.api.Test
//    void isNotEmpty() {
//        Assertions.assertTrue(LX.isNotEmpty("123"));
//        Assertions.assertTrue(LX.isNotEmpty(new Object()));
//    }
//
//    @org.junit.jupiter.api.Test
//    void exMsg() {
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exMsg("错误");});
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void testExMsg() {
//        Assertions.assertThrowsExactly(RuntimeException.class,()->{LX.exMsg(new RuntimeException("RuntimeException"));});
//    }
//
//    @org.junit.jupiter.api.Test
//    void testExMsg1() {
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exMsg("错误",123);});
//    }
//
//    @org.junit.jupiter.api.Test
//    void testExMsg2() {
//        LX.exMsg(false,"正确");
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exMsg(true,"错误");});
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void testExMsg3() {
//        LX.exMsg(false,"正确",123);
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exMsg(true,"错误",123);});
//    }
//
//    @org.junit.jupiter.api.Test
//    void exObj() {
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exObj(null);});
//    }
//
//    @org.junit.jupiter.api.Test
//    void testExObj() {
//        Assertions.assertThrowsExactly(ResultServiceException.class,()->{LX.exObj(null,"空的!");});
//        LX.exObj("null","不是空的!");
//    }
//
//    @org.junit.jupiter.api.Test
//    void uuid() {
//        System.out.println(LX.uuid());
//        Assertions.assertEquals(36, LX.uuid().length());
//    }
//
//    @org.junit.jupiter.api.Test
//    void testUuid() {
//        System.out.println(LX.uuid(0));
//        System.out.println(LX.uuid(100));
//        System.out.println(LX.uuid(10));
//        Assertions.assertEquals(36, LX.uuid(0).length());
//        Assertions.assertEquals(36, LX.uuid(100).length());
//        Assertions.assertEquals(10, LX.uuid(10).length());
//    }
//
//    @org.junit.jupiter.api.Test
//    void uuid32() {
//        System.out.println(LX.uuid32(0));
//        Assertions.assertEquals(32, LX.uuid32().length());
//    }
//
//    @org.junit.jupiter.api.Test
//    void testUuid32() {
//        System.out.println(LX.uuid32(0));
//        System.out.println(LX.uuid32(100));
//        System.out.println(LX.uuid32(10));
//        Assertions.assertEquals(32, LX.uuid32(0).length());
//        Assertions.assertEquals(32, LX.uuid32(100).length());
//        Assertions.assertEquals(10, LX.uuid32(10).length());
//    }
//
//    @org.junit.jupiter.api.Test
//    void primaryid() {
//        long l = System.currentTimeMillis();
//        Set<Long> set = new HashSet<>(1000000);
//        for (int i = 0; i < 1000000; i++) {
//            set.add(LX.snowid());
//        }
//        System.out.println(System.currentTimeMillis()-l);
//        System.out.println(set.size());
//    }
//
//    public class A{
//        private String a;
//        private int x;
//
//        public String getA() {
//            return a;
//        }
//
//        public void setA(String a) {
//            this.a = a;
//        }
//
//        public int getX() {
//            return x;
//        }
//
//        public void setX(int x) {
//            this.x = x;
//        }
//
//        @Override
//        public String toString() {
//            return "{" +
//                    "a='" + a + '\'' +
//                    ", x=" + x +
//                    '}';
//        }
//    }
//    @org.junit.jupiter.api.Test
//    void toMap() {
//
//        A a = new A();
//        a.setA("123");
//        Var var = LX.toMap(a);
//        System.out.println(var);
//    }
//
//    @org.junit.jupiter.api.Test
//    void testToMap() {
//        HashMap hashMap = LX.toMap(HashMap.class, "{a:123, x:0}");
//        System.out.println(hashMap);
//    }
//
//    @org.junit.jupiter.api.Test
//    void toObj() {
//        System.out.println(LX.toObj(A.class, "{a:123, x:0}"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void toList() {
//        System.out.println(LX.toList("[0,1,12]").get(0));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testToList() {
//        System.out.println(LX.toList(Var.class, "[{a:123,x:1}]").get(0).getStr("a"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void toJSONString() {
//        System.out.println(LX.toJSONString(new Var("{a:123,x:1}")));
//    }
//
//    @org.junit.jupiter.api.Test
//    void toFormatJson() {
//        System.out.println(LX.toFormatJson(new Var("{a:123,x:1.2,y:'消息',1:2}")));
//    }
//
//    @org.junit.jupiter.api.Test
//    void doPost() {
//        System.out.println(LX.doPost("https://www.baidu.com",""));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testDoPost() {
//        System.out.println(LX.doPost("https://www.baidu.com","",null));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testDoPost1() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void testDoPost2() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void doGet() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void testDoGet() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void doGetStream() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void doPostInputStream() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void encode() {
//        System.out.println(LX.encode("123阿斯顿XXX?><>??+_@_#@"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void decode() {
//        System.out.println(LX.decode(LX.encode("123阿斯顿XXX?><>??+_@_#@")));
//    }
//
//    @org.junit.jupiter.api.Test
//    void base64Encode() {
//        System.out.println(LX.base64Encode("123阿斯顿XXX?><>??+_@_#@".getBytes(StandardCharsets.UTF_8)));
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void base64Decode() {
//        System.out.println(new String(LX.base64Decode(LX.base64Encode("123阿斯顿XXX?><>??+_@_#@".getBytes(StandardCharsets.UTF_8)))));
//    }
//
//    @org.junit.jupiter.api.Test
//    void md5() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void createLinkString() {
//        System.out.println(LX.createLinkString(new Var("{a:'123',x:'1.2',y:'消息',1:'2'}")));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testCreateLinkString() {
//        System.out.println(LX.createLinkString(new Var("{a:'123',x:'',y:'消息',1:'2'}"),"1","##",false,false));
//    }
//
//    @org.junit.jupiter.api.Test
//    void rsaSign() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void rsaEncrypt() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void reaVerify() {
//        String publicStr = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1pQ7WTJGi4n6L+IfejvIBEV+dDD/CdQ4ESLjOZrqHgjPhC+WJTMLcv5gO/N5RxyXx3CNK/GnCOPnX3SmUHtv/NkQmeiZwFyZD0HQ09OBgo7/D00zFrQHU6uVxQ1O2n4CeQ4lr95TTAjDIXGhQSvjl8lFFezVSNpyGfOiw/e19URrnVmjkdrH+1PNtQ+q+32h6z74yRsQhd36VhcjEgSlSfrXII82aQmaRB0IQctM+TNmls8b/D6pxvyCj5KFj5Fy23c19NFv1Bs1f9e2DFGjK1cR8rENJQ98yZNyzw+yDD151MdkiqilaH12eRaAX6WsjA92ahoi6AnL3V4z4owirwIDAQAB";
//        String privateStr = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDWlDtZMkaLifov4h96O8gERX50MP8J1DgRIuM5muoeCM+EL5YlMwty/mA783lHHJfHcI0r8acI4+dfdKZQe2/82RCZ6JnAXJkPQdDT04GCjv8PTTMWtAdTq5XFDU7afgJ5DiWv3lNMCMMhcaFBK+OXyUUV7NVI2nIZ86LD97X1RGudWaOR2sf7U821D6r7faHrPvjJGxCF3fpWFyMSBKVJ+tcgjzZpCZpEHQhBy0z5M2aWzxv8PqnG/IKPkoWPkXLbdzX00W/UGzV/17YMUaMrVxHysQ0lD3zJk3LPD7IMPXnUx2SKqKVofXZ5FoBfpayMD3ZqGiLoCcvdXjPijCKvAgMBAAECggEBAKZzuk4I1/xqfe8jA7P4J8JYfi13zH89Zni0jXskFrRRtVge6ePVYu5DMTeQOOXW4k5NmtUZ0a7ty4+al2EbP9XuNBiP/KThr8HMFkgba/1xG54tDpJv+LcJBVckaaTH1vc86DMyzX7P1T5ePO6nMxeNg8/sNc8b1SMYgYxu3XFxw3uUiUpGfZrz6GQ2zCiGCGm/XKs+v3/xBjbkggx9m95XutXMO0mMEwbgb/DSxzHPiTrEf2O3m1F0WmCBhU+OSG99gCLZbPNjr+UvTckh3r/GDDp9ooSNOxLRs/eXhr3OzxzRdnjWpFU6KB5OPasKh2Jg1lvcga34ApDgSaLTomECgYEA+ijUNeSVLpuS6ETsJTdCs3dd3/u5w885ZPhoWzHoEjM1eTHDxiKDASfLSaWNZO18JLP7VO7ULp0t3QKVnZYXa0xHCVvBTBSZ8PR/TlrFH5++m6CMKjH9gwsgA9HFJftovyUViEXVQEdd+1opMzSIECmAUk0tpwOsZcOhf2Qjj4cCgYEA25a+OcQIrdJGkImDIGnPWJqx4YdG//9idIObdZ5tzp6+jYgdBRvgnyib3+Y9zj2I0FTnulJB+DEmmXDZd2xhkQ12FyT8Cdxn9mCWRxSuduhgqFoXSQpWAinJMixGo0iBwgzcUGsmsrqLlqxdfBgCdO53nGuLmewH1MdUiTZSjZkCgYBIF9Iogeq4Vt98jrhbdVSEsgbc0QfVNXg98BTrCzPtYtKrTbs086c7gwNkAxD/oC1hynjJHlcJ5AReqCsEEkhHhOEIAmhvi5Rdy6iQpTogBgKBiUA7vNJpnKAqWZ7udkjWpVJSRoXRk3+zO2WaOy5nMf6oOVPJyRY68akuNGbHKwKBgBXHaa6JBuNWi7EiMmMwm2QKU8IujJgvWDB+LRkJUZLAo92dtmMfVLP0byFR0Kd36u3evez445oeJdnqG2eA1FxynVfGT0kbFySUh9Mc9gDNI2kPJmgUIluskUvsoETpmBUK+X7wYbIwVk/nYI5CQ1wdmrdk4z+s7UnpSzUgsidhAoGAEaENGMjKICahRM6ZLQ8h8Eu38WuOvH4YAQXxbNxyT81KEv5fBkgrZ7BZK40/8PztytY/d/yhaSLLDfDizPkk/TjbK64QxwDLM8Etu8BBDEuDsCRrLpq2Q+phkmTq8PfDbUoV2dWFTy8twJf09+R2OQjtIDwWSBIojN+P6mxI+CU=";
//        Var var = new Var("{a:'123',x:'',y:'消息',1:'2'}");
//        String sign = LX.rsaSign(var, privateStr);
//        var.put("sign1",sign);
//        System.out.println(LX.rsaVerify(var,sign,publicStr));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testReaVerify() {
//        String publicStr = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1pQ7WTJGi4n6L+IfejvIBEV+dDD/CdQ4ESLjOZrqHgjPhC+WJTMLcv5gO/N5RxyXx3CNK/GnCOPnX3SmUHtv/NkQmeiZwFyZD0HQ09OBgo7/D00zFrQHU6uVxQ1O2n4CeQ4lr95TTAjDIXGhQSvjl8lFFezVSNpyGfOiw/e19URrnVmjkdrH+1PNtQ+q+32h6z74yRsQhd36VhcjEgSlSfrXII82aQmaRB0IQctM+TNmls8b/D6pxvyCj5KFj5Fy23c19NFv1Bs1f9e2DFGjK1cR8rENJQ98yZNyzw+yDD151MdkiqilaH12eRaAX6WsjA92ahoi6AnL3V4z4owirwIDAQAB";
//        String privateStr = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDWlDtZMkaLifov4h96O8gERX50MP8J1DgRIuM5muoeCM+EL5YlMwty/mA783lHHJfHcI0r8acI4+dfdKZQe2/82RCZ6JnAXJkPQdDT04GCjv8PTTMWtAdTq5XFDU7afgJ5DiWv3lNMCMMhcaFBK+OXyUUV7NVI2nIZ86LD97X1RGudWaOR2sf7U821D6r7faHrPvjJGxCF3fpWFyMSBKVJ+tcgjzZpCZpEHQhBy0z5M2aWzxv8PqnG/IKPkoWPkXLbdzX00W/UGzV/17YMUaMrVxHysQ0lD3zJk3LPD7IMPXnUx2SKqKVofXZ5FoBfpayMD3ZqGiLoCcvdXjPijCKvAgMBAAECggEBAKZzuk4I1/xqfe8jA7P4J8JYfi13zH89Zni0jXskFrRRtVge6ePVYu5DMTeQOOXW4k5NmtUZ0a7ty4+al2EbP9XuNBiP/KThr8HMFkgba/1xG54tDpJv+LcJBVckaaTH1vc86DMyzX7P1T5ePO6nMxeNg8/sNc8b1SMYgYxu3XFxw3uUiUpGfZrz6GQ2zCiGCGm/XKs+v3/xBjbkggx9m95XutXMO0mMEwbgb/DSxzHPiTrEf2O3m1F0WmCBhU+OSG99gCLZbPNjr+UvTckh3r/GDDp9ooSNOxLRs/eXhr3OzxzRdnjWpFU6KB5OPasKh2Jg1lvcga34ApDgSaLTomECgYEA+ijUNeSVLpuS6ETsJTdCs3dd3/u5w885ZPhoWzHoEjM1eTHDxiKDASfLSaWNZO18JLP7VO7ULp0t3QKVnZYXa0xHCVvBTBSZ8PR/TlrFH5++m6CMKjH9gwsgA9HFJftovyUViEXVQEdd+1opMzSIECmAUk0tpwOsZcOhf2Qjj4cCgYEA25a+OcQIrdJGkImDIGnPWJqx4YdG//9idIObdZ5tzp6+jYgdBRvgnyib3+Y9zj2I0FTnulJB+DEmmXDZd2xhkQ12FyT8Cdxn9mCWRxSuduhgqFoXSQpWAinJMixGo0iBwgzcUGsmsrqLlqxdfBgCdO53nGuLmewH1MdUiTZSjZkCgYBIF9Iogeq4Vt98jrhbdVSEsgbc0QfVNXg98BTrCzPtYtKrTbs086c7gwNkAxD/oC1hynjJHlcJ5AReqCsEEkhHhOEIAmhvi5Rdy6iQpTogBgKBiUA7vNJpnKAqWZ7udkjWpVJSRoXRk3+zO2WaOy5nMf6oOVPJyRY68akuNGbHKwKBgBXHaa6JBuNWi7EiMmMwm2QKU8IujJgvWDB+LRkJUZLAo92dtmMfVLP0byFR0Kd36u3evez445oeJdnqG2eA1FxynVfGT0kbFySUh9Mc9gDNI2kPJmgUIluskUvsoETpmBUK+X7wYbIwVk/nYI5CQ1wdmrdk4z+s7UnpSzUgsidhAoGAEaENGMjKICahRM6ZLQ8h8Eu38WuOvH4YAQXxbNxyT81KEv5fBkgrZ7BZK40/8PztytY/d/yhaSLLDfDizPkk/TjbK64QxwDLM8Etu8BBDEuDsCRrLpq2Q+phkmTq8PfDbUoV2dWFTy8twJf09+R2OQjtIDwWSBIojN+P6mxI+CU=";
//        Var var = new Var("{a:'123',x:'',y:'消息',1:'2'}");
//        String sign = LX.rsaEncrypt(LX.createLinkString(var,"sign1","##",true,true), privateStr);
//        var.put("sign1",sign);
//        System.out.println(LX.rsaVerify(LX.createLinkString(var,"sign1","##",true,true),sign,publicStr));
//    }
//
//    @org.junit.jupiter.api.Test
//    void desEncrypt() {
//        System.out.println(LX.desEncrypt("123阿斯顿XXX?><>??+_@_#@","xxxx1111"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void desDecrypt() {
//        byte[] bytes = LX.desEncrypt("123阿斯顿XXX?><>??+_@_#@".getBytes(StandardCharsets.UTF_8), "xxxx1111", "xxxx1111", CIPHER_ALGORITHM, Charset.forName("UTF-8"));
//        String data = LX.toHexString(bytes);
//        System.out.println(data);
//        System.out.println(new String(LX.desDecrypt(LX.convertHexString(data),"xxxx1111","xxxx1111",CIPHER_ALGORITHM, Charset.forName("UTF-8"))));
//    }
//
//    @org.junit.jupiter.api.Test
//    void sleep() {
//        System.out.println(LX.getTime());
//        LX.sleep(1000);
//        System.out.println(LX.getTime());
//    }
//
//    @org.junit.jupiter.api.Test
//    void urlEncode() {
//        System.out.println(LX.urlEncode("啊大苏打撒旦+-*/{[]"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void toByteArray() {
//        System.out.println(new String(LX.inputStreamToByteArray(new ByteArrayInputStream("行啊大苏打".getBytes(StandardCharsets.UTF_8)))));
//    }
//
//    @org.junit.jupiter.api.Test
//    void streamToString() {
//        System.out.println(LX.inputStreamToString(new ByteArrayInputStream("行啊大苏打".getBytes(StandardCharsets.UTF_8))));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testStreamToString() {
//        System.out.println(LX.inputStreamToString(new ByteArrayInputStream("行啊大苏打".getBytes(StandardCharsets.UTF_8)),Charset.forName("GBK")));
//    }
//
//    @org.junit.jupiter.api.Test
//    void toXml() {
//        Var var = new Var("{a:'123',x:'',y:'消息',b:'2'}");
//        var.put("var",new Var("{v:1}"));
//        var.put("ls",LX.toList("[{a:1,b:gf},{a:2}]"));
//        System.out.println(LX.toXml(var,"xml",false));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testToXml() {
//        Var var = new Var("{a:'123',x:'',y:'消息',b:'2'}");
//        var.put("ls1",LX.toList("[{a:1}]"));
//        List ls = new ArrayList<>();
//        ls.add(var);
//        ls.add(var);
//        System.out.println(LX.toXml(ls,"","xx",false));
//    }
//
//    @org.junit.jupiter.api.Test
//    void xmlToMap() {
//        String xml1 =
//                "<xml>\n" +
//                "    <x><a>12.3</a><a>12.5</a></x>\n" +
//                "    <a>123</a>\n" +
//                "    <y>消息</y>\n" +
//                "    <b></b>\n" +
//                "</xml>";
//        System.out.println(LX.xmlToMap(xml1).get("x").getMap().get("a"));
//
//        String xml =
//                "<root>\n" +
//                "    <xml>\n" +
//                "        <x></x>\n" +
//                "        <a>123</a>\n" +
//                "        <y>消息</y>\n" +
//                "        <b>2</b>\n" +
//                "    </xml>\n" +
//                "    <xml>\n" +
//                "        <x></x>\n" +
//                "        <a>123</a>\n" +
//                "        <y>消息</y>\n" +
//                "        <b>2</b>\n" +
//                "    </xml>\n" +
//                "</root>";
//        System.out.println(LX.xmlToList(xml).get(1).getMap().get("y"));
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void initialLower(){
//    }
//
//    @org.junit.jupiter.api.Test
//    void initialUp() {
//        System.out.println(LX.initialUp("asdcas"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void str() {
//        System.out.println(LX.str(null));
//    }
//
//    @org.junit.jupiter.api.Test
//    void compareTo() {
//        boolean b = LX.compareTo("2", "3", MathUtil.Type.GT);
//        System.out.println(b);
//    }
//
//    @org.junit.jupiter.api.Test
//    void getTime() {
//        System.out.println(LX.getTime());
//    }
//
//    @org.junit.jupiter.api.Test
//    void getDay() {
//        System.out.println(LX.getDay());
//    }
//
//    @org.junit.jupiter.api.Test
//    void getDate() {
//        System.out.println(LX.getDate("yyyy-mm:ss"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void testGetDate() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void isNotEmptyMap() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void exMap() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void exEntity() {
//    }
//
//    @org.junit.jupiter.api.Test
//    void isNum() {
//        System.out.println(LX.isNum("1.23"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void eval() {
//        System.out.println(LX.eval("1+2*3-(2-1)*5"));
//    }
//
//    @org.junit.jupiter.api.Test
//    void getBigDecimal() {
//        System.out.println(LX.getBigDecimal("0.0001").add(LX.getBigDecimal("1.3")));
//    }
//
//    @Test
//    void expireCache(){
//        ExpireCache expireCache = new ExpireCache(100000,3000);
//        expireCache.put("a",1);
//        Assertions.assertEquals(expireCache.get("a"),1);
//        LX.sleep(2000);
//        Assertions.assertEquals(expireCache.get("a"),1);
//        LX.sleep(1000);
//        Assertions.assertNull(expireCache.get("a"));
//        expireCache.get("a","2");
//        Assertions.assertEquals(expireCache.get("a"),"2");
//        expireCache.remove("a");
//        Assertions.assertNull(expireCache.get("a"));
//        expireCache.get("a",()->"3");
//        Assertions.assertEquals(expireCache.get("a"),"3");
//        expireCache.clear();
//        Assertions.assertEquals(0, expireCache.size());
//        expireCache.put(null,null);
//        expireCache.get(null);
//        expireCache.get(null,null);
//        expireCache.get(null,()->null);
//        Assertions.assertEquals(0, expireCache.size());
//        expireCache.put("a",null);
//        Assertions.assertEquals(1, expireCache.size());
//        Random random = new Random();
//        long t = System.currentTimeMillis();
//        for (int i = 0; i < 10000000; i++) {
//            expireCache.put(random.nextInt(100000), i);
//        }
//        System.out.println(System.currentTimeMillis() - t);
//        for (int i = 0; i < 10000000; i++) {
//            expireCache.get(random.nextInt(100000));
//        }
//        System.out.println(System.currentTimeMillis() - t);
//        Assertions.assertTrue((System.currentTimeMillis() -t)<3000,"时间太长");
//
//
//    }
//
//}