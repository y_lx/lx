# lx工具类
```
# maven引入:

<dependency>
    <groupId>io.github.52ylx</groupId>
    <artifactId>lx</artifactId>
    <version>2.0.2</version>
</dependency>
```

```
提供了一个Map子类:  com.lx.entity.Var
引用了Gson的源码并进行的一定调整(支持Long类型)来支持对象类型转换

集成了工作中常用的方法:  com.lx.util.LX
1. 判断对象是否为空: 
    null    
    | Map.size=0    
    | Collection.size=0 
    | Array[].length=0 
    | String = '' 
LX.isEmpty  LX.isNotEmpty

2. 抛异常  异常类型: com.lx.util.exception.ResultServiceException
LX.exMsg  直接抛异常 或者 为 true 抛异常
LX.exObj  对象为空抛异常

3. 生成 uuid 和 雪花ID
LX.uuid     可以指定uuid长度 0-36
LX.uuid32   可以指定uuid长度 0-32
LX.snowid   雪花ID

4. 对象类型转换
LX.toMap
LX.toList
LX.toObj

5. 转json字符串
LX.toJSONString
LX.toFormatJson  格式化的json

6. 调用HTTP
LX.doPost
LX.doGet
LX.doPut
LX.doDelete
LX.doGetStream       返回值为InputStream
LX.doPostInputStream 返回值为InputStream
LX.httpInputStream   返回值为InputStream

7. base64 及 md5
LX.encode
LX.decode
LX.base64Encode
LX.base64Decode
LX.md5

8. RSA签名 及 DES加密
LX.createLinkString  字典序拼接待签名的Map
LX.rsaSign
LX.rsaEncrypt
LX.rsaVerify
LX.desEncrypt
LX.desDecrypt

9. byte[] 与 16进制字符串 互转
LX.toHexString
LX.convertHexString

10. URL编码
LX.urlEncode
LX.urlDecode

11. InputStream -> byte[] ,String ,OutputStream
LX.inputStreamToByteArray
LX.inputStreamToString
LX.inputStreamToOutputStream

12. xml 与 对象 互转
LX.toXml
LX.xmlToStringMap
LX.xmlToMap
LX.xmlToList

13. 数字操作
LX.compareTo        比较大小
LX.isNum            判断是否为数字
LX.eval             计算字符串表达式 +-*/
LX.getBigDecimal    转BigDecimal

14. 日期时间获取
LX.getTime          获取日期时间 yyyy-MM-dd HH:mm:ss
LX.getDay           获取日期 yyyy-MM-dd
LX.getDate          获取指定格式时间 

15.工具
LX.sleep            睡眠n毫秒
LX.initialLower     首字符小写
LX.initialUp        首字符大写
LX.str              对象为null转字符串为""
LX.left             n个字符左对齐
LX.right            n个字符右对齐


```